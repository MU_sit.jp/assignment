import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AssignmentGUI {
    private JPanel root;
    private JLabel TopLabel;
    private JButton tunaButton;
    private JButton salmonButton;
    private JButton omeletButton;
    private JButton shrimpButton;
    private JButton octopusButton;
    private JButton nattoButton;
    private JTextPane textPane1;
    private JButton checkOutButton;
    private JLabel Totalprice;

    int total = 0;


    void order(String Neta, int price){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + Neta + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if( confirmation == 0){
            Object[] possibleValues = { "with wasabi", "without wasabi" };
            Object selectedValue = JOptionPane.showInputDialog(null,
                    "Choose one",
                    "Confirm",
                    JOptionPane.INFORMATION_MESSAGE,
                    null,
                    possibleValues, possibleValues[0]);
            if ( selectedValue == "with wasabi" ){
                JOptionPane.showMessageDialog(null,
                        "Thank you for ordering " + Neta + " (with wasabi) ! It will be served as soon as possible.");
            }else if( selectedValue == "without wasabi"){
                JOptionPane.showMessageDialog(null,
                        "Thank you for ordering " + Neta + " (without wasabi) ! It will be served as soon as possible.");
            }else{
                return;
            }

            String currentText = textPane1.getText();
            textPane1.setText( currentText + Neta + "  " + price + "yen" + "\n" );
            total = total + price;
            Totalprice.setText( "Total      " + total + " yen" );
        }
    }


    public AssignmentGUI() {
        tunaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tuna",110);
            }
        });
        tunaButton.setIcon(new ImageIcon(
                this.getClass().getResource("maguro.jpg")
        ));
        salmonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Salmon",120);
            }
        });
        salmonButton.setIcon(new ImageIcon(
                this.getClass().getResource("samon.png")
        ));
        omeletButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Omelet",130);
            }
        });
        omeletButton.setIcon(new ImageIcon(
                this.getClass().getResource("tamago.jpg")
        ));
        shrimpButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Shrimp",140);
            }
        });
        shrimpButton.setIcon(new ImageIcon(
                this.getClass().getResource("ebi.jpg")
        ));
        octopusButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Octopus",150);
            }
        });
        octopusButton.setIcon(new ImageIcon(
                this.getClass().getResource("tako.jpg")
        ));
        nattoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Natto",160);
            }
        });
        nattoButton.setIcon(new ImageIcon(
                this.getClass().getResource("natto.jpg")
        ));
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation1 = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if( confirmation1 == 0){
                    JOptionPane.showMessageDialog(null,
                            "Thank you!! The total price is " + total + "yen." + "\n\n" + "Have a great day!!");
                    textPane1.setText( null );
                    total = 0;
                    Totalprice.setText( "Total      " + total + " yen" );
                }

            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("AssignmentGUI");
        frame.setContentPane(new AssignmentGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}

